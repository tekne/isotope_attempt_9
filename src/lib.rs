use petgraph::stable_graph::StableDiGraph;
use petgraph::graph::NodeIndex;
use typed_generational_arena::{NonZeroIndex, IgnoreGeneration, Arena, Index};
use internship::IStr;
use std::collections::HashMap;

pub type ScopeIx = usize;
pub type ValueIx = usize;
pub type UniverseIx = usize;
pub type ParamIx = u32;

/// An index into the arena for value metadata
pub type MetaIx = Index<ValueMetadata, NonZeroIndex<usize>, IgnoreGeneration>;
/// The default capacity for nodes in the scope dependency graph
pub const DEFAULT_SCOPE_NODE_CAP : usize = 100;
/// The default capacity for edges in the scope dependency graph
pub const DEFAULT_SCOPE_EDGE_CAP : usize = 300;
/// The default capacity for nodes in the value definition graph
pub const DEFAULT_VALUE_NODE_CAP : usize = 1000;
/// The default capacity for edges in the value definition graph
pub const DEFAULT_VALUE_EDGE_CAP : usize = 5000;
/// The default capacity for nodes in the universe dependency graph
pub const DEFAULT_UNIVERSE_NODE_CAP : usize = 100;
/// The default capacity for edges in the universe dependency graph
pub const DEFAULT_UNIVERSE_EDGE_CAP : usize = 300;
/// The default capacity of the arena for value metadata
pub const DEFAULT_META_CAP : usize = 100;

/// An isotope context, containing all compiled scopes *and* (public) value allocations so far
#[derive(Debug, Clone)]
pub struct Context {
    scope_deps : StableDiGraph<Scope, (), ScopeIx>,
    value_graph : StableDiGraph<Value, Selector, ValueIx>,
    universe_deps : StableDiGraph<(), (), UniverseIx>,
    value_meta : Arena<ValueMetadata, NonZeroIndex<usize>, IgnoreGeneration>
}

impl Context {
    /// Create an empty `Context` with the given capacities
    pub fn with_capacities(
        scope_node_cap: usize, scope_edge_cap: usize,
        value_node_cap: usize, value_edge_cap: usize,
        universe_node_cap: usize, universe_edge_cap: usize,
        meta_cap: usize
    ) -> Context {
        let mut universe_deps = StableDiGraph::with_capacity(universe_node_cap, universe_edge_cap);
        // The "base universe"
        universe_deps.add_node(());
        Context {
            scope_deps : StableDiGraph::with_capacity(scope_node_cap, scope_edge_cap),
            value_graph : StableDiGraph::with_capacity(value_node_cap, value_edge_cap),
            value_meta : Arena::with_capacity(meta_cap),
            universe_deps
        }
    }
    /// Create a new `Scope` in a given `Context`, with optional metadata
    pub fn new_scope(&mut self, owner : Option<ValueId>, meta : Option<ValueMetadata>) -> ScopeId {
        let scope = Scope {
            id : ScopeId(NodeIndex::new(0)),
            name_map : HashMap::new(),
            meta : meta.map(|data| self.value_meta.insert(data)),
            owner
        };
        let id = ScopeId(self.scope_deps.add_node(scope));
        self.scope_deps[id.0].id = id;
        id
    }
    /// Create a new typing universe in a given `Context`
    #[inline] pub fn new_universe(&mut self) -> UniverseId {
        UniverseId(self.universe_deps.add_node(())) }
    /// Get this `Context's` base universe
    #[inline] pub fn base_universe(&self) -> UniverseId { UniverseId(NodeIndex::new(0)) }
    /// Borrow a `Scope` from this `Context` given its `ScopeId`
    #[inline]
    pub fn borrow_scope(&mut self, sc : ScopeId) -> ScopeGraph {
        ScopeGraph {
            values : &mut self.value_graph,
            scope : &mut self.scope_deps[sc.0],
            meta : &mut self.value_meta
        }
    }
}

impl Default for Context {
    /// Create an empty context with the default capacities
    fn default() -> Self { Self::with_capacities(
        DEFAULT_SCOPE_NODE_CAP, DEFAULT_SCOPE_EDGE_CAP,
        DEFAULT_VALUE_NODE_CAP, DEFAULT_VALUE_EDGE_CAP,
        DEFAULT_UNIVERSE_NODE_CAP, DEFAULT_UNIVERSE_EDGE_CAP,
        DEFAULT_META_CAP ) }
}

/// A selector for an isotope value edge, choosing which value to extract from
/// a node and which slot to insert the value in
#[derive(Debug, Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Default)]
pub struct Selector {
    /// The value to extract
    pub input :  ParamIx,
    /// The slot to pass it to
    pub output : ParamIx
}

/// The identifier of a polymorphic universe
#[derive(Debug, Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash)]
pub struct UniverseId(NodeIndex<UniverseIx>);

/// The identifier of a parameter to an isotope function
#[derive(Debug, Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash)]
pub struct ParamId(ParamIx);

/// The identifier of an isotope scope
#[derive(Debug, Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash)]
pub struct ScopeId(NodeIndex<ScopeIx>);

/// The identifier of an isotope value
#[derive(Debug, Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash)]
pub struct ValueId(NodeIndex<ValueIx>);

/// An isotope scope
#[derive(Debug, Clone)]
pub struct Scope {
    /// The index of this scope in the dependency graph
    id: ScopeId,
    /// The value owning this scope, if any. If so, then the owner object
    /// *only* can access values within this scope even from an outer scope.
    owner: Option<ValueId>,
    /// A map of names to the *latest* variables having those names
    name_map: HashMap<IStr, ValueId>,
    /// Metadata for this scope, taken as a value, if any
    meta: Option<MetaIx>
}

/// An isotope value node
#[derive(Debug, Clone)]
pub struct Value {
    /// The raw data of this value
    raw: RawValue,
    /// The index of this value
    val_id: ValueId,
    /// The scope this value is contained in
    scope_id: ScopeId,
    /// The index of the metadata of this value, if any
    meta: Option<MetaIx>
}

#[derive(Debug)]
pub struct ScopeGraph<'a> {
    /// The values of the borrowed context
    values: &'a mut StableDiGraph<Value, Selector, ValueIx>,
    /// The borrowed scope of the borrowed context
    scope: &'a mut Scope,
    /// The metadata of the borrowed context
    meta: &'a mut Arena<ValueMetadata, NonZeroIndex<usize>, IgnoreGeneration>
}

impl<'a> ScopeGraph<'a> {
    /// Create a new value in the borrowed scope
    pub fn new_value<S: Into<IStr>>(&mut self, raw: RawValue, name : Option<S>) -> ValueId {
        let name : Option<IStr> = name.map(|s| s.into());
        let meta = name.as_ref().map(|name| {
            let counter = self.scope.name_map.get(name)
                .map(|val| self.values[val.0].meta)
                .unwrap_or(None)
                .map(|meta| self.meta[meta].counter + 1)
                .unwrap_or(0);
            ValueMetadata { name : name.clone(), counter }
        });
        let new_value = Value {
            raw,
            val_id : ValueId(NodeIndex::new(0)),
            scope_id : self.scope.id,
            meta : meta.map(|data| self.meta.insert(data))
        };
        let id = ValueId(self.values.add_node(new_value));
        self.values[id.0].val_id = id;
        name.map(|name| self.scope.name_map.insert(name, id));
        id
    }
}

/// An isotope value node with no associated metadata
#[derive(Debug, Clone)]
pub enum RawValue {
    /// A constant
    Constant(Constant),
    /// A new basic block
    Block(NodeIndex<ScopeIx>),
    /// A parameter to a basic block
    Parameter(ParamId),
    /// An evaluation (as an S-expression).
    /// The empty eval (no arguments) represents the null tuple.
    Eval,
    /// A tuple unpacking.
    Unpack,
    /// A tuple packing.
    Pack,
}

/// The metadata potentially associated with an isotope value node
#[derive(Debug, Clone)]
pub struct ValueMetadata {
    /// The name of this value
    name : IStr,
    /// The name counter of this value (e.g. `1x` `2x` `3x` for temporary values of `x`).
    /// 0 is not displayed, i.e. `0x` shows as `x`.
    counter : u32
}

/// An isotope constant
#[derive(Debug, Clone)]
pub enum Constant {
    /// A small integer
    SmallInteger(i64),
    /// A small natural number
    SmallNatural(u64),
    /// A single byte
    Byte(u8),
    /// A (polymorphic) typing universe
    Universe(UniverseId)
    //TODO: more?
}
